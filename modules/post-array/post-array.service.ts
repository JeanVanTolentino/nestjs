import { Injectable } from '@nestjs/common';
import { CreatePostArrayDto } from './dto/create-post-array.dto';
import { UpdatePostArrayDto } from './dto/update-post-array.dto';

@Injectable()
export class PostArrayService {
  create(createPostArrayDto: CreatePostArrayDto) {
    return 'This action adds a new postArray';
  }

  findAll() {
    return `This action returns all postArray`;
  }

  findOne(id: number) {
    return `This action returns a #${id} postArray`;
  }

  update(id: number, updatePostArrayDto: UpdatePostArrayDto) {
    return `This action updates a #${id} postArray`;
  }

  remove(id: number) {
    return `This action removes a #${id} postArray`;
  }
}
