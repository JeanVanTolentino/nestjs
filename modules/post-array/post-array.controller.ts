import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { PostArrayService } from './post-array.service';
import { CreatePostArrayDto } from './dto/create-post-array.dto';
import { UpdatePostArrayDto } from './dto/update-post-array.dto';

@Controller('post-array')
export class PostArrayController {
  constructor(private readonly postArrayService: PostArrayService) {}

  @Post()
  create(@Body() createPostArrayDto: CreatePostArrayDto) {
    return this.postArrayService.create(createPostArrayDto);
  }

  @Get()
  findAll() {
    return this.postArrayService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.postArrayService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePostArrayDto: UpdatePostArrayDto) {
    return this.postArrayService.update(+id, updatePostArrayDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.postArrayService.remove(+id);
  }
}
