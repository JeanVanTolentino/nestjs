import { Test, TestingModule } from '@nestjs/testing';
import { PostArrayService } from './post-array.service';

describe('PostArrayService', () => {
  let service: PostArrayService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PostArrayService],
    }).compile();

    service = module.get<PostArrayService>(PostArrayService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
