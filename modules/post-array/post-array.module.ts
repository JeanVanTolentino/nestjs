import { Module } from '@nestjs/common';
import { PostArrayService } from './post-array.service';
import { PostArrayController } from './post-array.controller';

@Module({
  controllers: [PostArrayController],
  providers: [PostArrayService]
})
export class PostArrayModule {}
