import { PartialType } from '@nestjs/mapped-types';
import { CreatePostArrayDto } from './create-post-array.dto';

export class UpdatePostArrayDto extends PartialType(CreatePostArrayDto) {}
