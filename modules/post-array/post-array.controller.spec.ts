import { Test, TestingModule } from '@nestjs/testing';
import { PostArrayController } from './post-array.controller';
import { PostArrayService } from './post-array.service';

describe('PostArrayController', () => {
  let controller: PostArrayController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PostArrayController],
      providers: [PostArrayService],
    }).compile();

    controller = module.get<PostArrayController>(PostArrayController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
